#  Vorlage Formelsammlung ET2 (IEM, THM)

Die Formelsammlung wird von Studierenden selbstständig erarbeitet. Jeder Lehrveranstaltungsteilnehmende kann dazu beitragen. Die Koordination der Beiträge übernimmt ein von den Studierenden im jeweiligen Semester bestimmter Maintainer.

Sie können diese Vorlage verwenden. Dazu erstellt der Maintainer einen Fork. 

Bitte beachten Sie folgende Regeln:
* Header und Lizenzhinweis (CC BY-SA 3.0) nicht entfernen
* Sie müssen mit den Lizenzbedingungen einverstanden sein, bevor Sie eigene Beiträge zum Dokument leisten.
* Der TeX-Code muss sich fehlerfrei übersetzen lassen; sollten Sie nicht übersetzbare Änderungen einfügen, erfolgt ggf. ein Ausschluss Ihrer Beiträge.

Die Formelsammlung darf zur Verwendung für die Prüfung ET2 maximal 4 Seiten umfassen. 
Sie wird vom Prüfer zusammen mit der Klausur ausgeteilt. Hierfür stimmt sich der Maintainer rechtzeitig vor der Klausur mit dem Prüfer ab, um den letzten Stand zu übermitteln.

Link zum .pdf der [letzten erstellten Version](https://git.thm.de/fmnk38/fs_elt2_template/-/jobs/artifacts/main/raw/fs_elt2_main.pdf?job=compile_pdf) sowie
[pdflatex Ausgabe](https://git.thm.de/fmnk38/fs_elt2_template/-/jobs/artifacts/main/raw/pdflatex_out.txt?job=compile_pdf)
oder [zip-File mit allen Dateien](https://git.thm.de/fmnk38/fs_elt2_template/-/jobs/artifacts/main/download?job=compile_pdf)
